package com.wtcweb.groupproject2017;

import android.app.*;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.wtcweb.groupproject2017.dialoglibrary.*;  // Custom Classes

import java.util.Calendar;

public class DialogActivity extends AppCompatActivity {

    private Button willBtn01;
    private Button willBtn02;

    private Button jeffBtn01;
    private Button jeffBtn02;

    private TextView tvDisplayDate;
    private android.app.DatePickerDialog.OnDateSetListener mDateSetListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        // Get references
        willBtn01 = (Button)findViewById(R.id.dialog01);
        willBtn02 = (Button)findViewById(R.id.dialog02);
        jeffBtn01 = (Button)findViewById(R.id.dialog03);
        jeffBtn02 = (Button)findViewById(R.id.btnDate);
        tvDisplayDate = (TextView) findViewById(R.id.tvDate);


        // Bind onClick event listeners
        willBtn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating the dialog fragment
                DialogFragment newDialog = new UserDialogFragment();

                //.show Dialog must be the last call because .show commits the transactions internally
                newDialog.show(getSupportFragmentManager(), "edit user");
            }
        });

        willBtn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newDialog = new UserListDialogFragment();
                newDialog.show(getSupportFragmentManager(), "user list");
            }
        });


        //basic alert
        jeffBtn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newDialog = new BasicAlert();
                newDialog.show(getSupportFragmentManager(), "Basic Alert");
            }
        });

        //dialog that sets up the date
        //had to use android.app.DatePicker because the code would not work otherwise
        jeffBtn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setting up calendar object
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        DialogActivity.this,
                        mDateSetListener,
                        year, month, day
                );

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();

            }
        });

        //creating the new datesetlistener
        mDateSetListener = new android.app.DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day){
                month = month + 1;

                String date = month + "/" + day + "/" + year;
                tvDisplayDate.setText(date);
            }
        };
    }
}
