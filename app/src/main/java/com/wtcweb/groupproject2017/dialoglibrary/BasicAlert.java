package com.wtcweb.groupproject2017.dialoglibrary;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import com.wtcweb.groupproject2017.R;

/**
 * Created by Jeffrey Young on 11/12/2017.
 */

public class BasicAlert extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //building the dialog
        //setting the title
        builder.setTitle("Basic Dialog");
        //setting the dialog message
        builder.setMessage("This is the message for a basic dialog");

        //creating te negative button such as cancel or exit
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getActivity(), "Negative button was clicked", Toast.LENGTH_SHORT).show();
            }
        });

        //setting the positive button such as save or ok
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getActivity(), "Positive button was clicked", Toast.LENGTH_SHORT).show();
            }
        });

        //creates the dialog
        Dialog dialog = builder.create();

        return dialog;
    }
}
