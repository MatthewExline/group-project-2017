package com.wtcweb.groupproject2017.dialoglibrary;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.wtcweb.groupproject2017.DialogActivity;
import com.wtcweb.groupproject2017.R;

/**
 * Created by William on 11/9/2017.
 */

public class UserListDialogFragment extends DialogFragment {

    private ListView listView;

    // Inner Classes for use by this Fragment only
    final static class ViewHolder{
        TextView lbl_name;
        TextView lbl_email;
        TextView lbl_music;
        TextView user_name;
        TextView user_email;
        TextView user_music;
        CheckBox user_active;
    }

    private final static class User {
        String name;
        String email;
        String favoriteMusic;
        boolean active;

        public User(String name, String email, String favoriteMusic, boolean active) {
            this.name = name;
            this.email = email;
            this.favoriteMusic = favoriteMusic;
            this.active = active;
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final User[] users = {
            new User("Bob", "bob@bob.com", "Jazz", true),
            new User("Carl", "carl@carl.com", "Rock", true),
            new User("Sally", "sally@sally.com", "Country", true),
            new User("Jim", "jim@jim.com", "Jazz", true)
        };

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View v = inflater.inflate(R.layout.user_list_dialog, null);
        builder.setView(v)
                .setTitle("User List")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // Get reference to the ListView
        listView = (ListView)v.findViewById(R.id.user_list_view);


        // Create an extended ArrayAdapter
        ArrayAdapter<User> userAdapter = new ArrayAdapter<User>(getActivity(), R.layout.user_list_dialog_item, users) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                User currentUser = users[position];

                if(convertView == null) {
                    convertView = getActivity().getLayoutInflater()
                            .inflate(R.layout.user_list_dialog_item, null, false);

                    // Fill the ViewHolder
                    ViewHolder viewHolder = new ViewHolder();
                    viewHolder.lbl_name = (TextView)convertView.findViewById(R.id.lblName);
                    viewHolder.lbl_email = (TextView)convertView.findViewById(R.id.lblEmail);
                    viewHolder.lbl_music = (TextView)convertView.findViewById(R.id.lblMusic);
                    viewHolder.user_name = (TextView)convertView.findViewById(R.id.user_list_view_name);
                    viewHolder.user_email = (TextView)convertView.findViewById(R.id.user_list_view_email);
                    viewHolder.user_music = (TextView)convertView.findViewById(R.id.user_list_view_music);
                    viewHolder.user_active = (CheckBox) convertView.findViewById(R.id.user_list_view_active);

                    // Store the ViewHolder Views into the convertView
                    convertView.setTag(viewHolder);
                }

                TextView lblName = ((ViewHolder)convertView.getTag()).lbl_name;
                TextView userName = ((ViewHolder)convertView.getTag()).user_name;
                userName.setText(currentUser.name);

                TextView lblEmail = ((ViewHolder)convertView.getTag()).lbl_email;
                TextView userEmail = ((ViewHolder)convertView.getTag()).user_email;
                userEmail.setText(currentUser.email);

                TextView lblMusic = ((ViewHolder)convertView.getTag()).lbl_music;
                TextView userMusic = ((ViewHolder)convertView.getTag()).user_music;
                userMusic.setText(currentUser.favoriteMusic);

                CheckBox userActive = ((ViewHolder)convertView.getTag()).user_active;
                userActive.setChecked(currentUser.active);

                return convertView;
            }
        };

        listView.setAdapter(userAdapter);




        // Create the AlertDialog object and return it
        return builder.create();
    }
}
