package com.wtcweb.groupproject2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MenusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menus);
    }


    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.imgGallery:
                startActivity(new Intent(this, ImgGalleryActivity.class));
                return true;
            case R.id.progressBar:
                startActivity(new Intent(this, ProgressBarActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
